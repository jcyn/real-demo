﻿using UnityEngine;
using System.Collections;

public class ToggleIcon : MonoBehaviour {

	public GameObject[] obj;
	public MovieTexture mov;

	void OnClick(){
		for (int i = 0; i < obj.Length; i++) {
			if (!obj[i].activeSelf && !mov.isPlaying) {
				obj[i].SetActive (true);
			} else {
				obj[i].SetActive (false);
			}
		}
	}
	void Update(){
		if (mov.isPlaying) {
			for (int i = 0; i < obj.Length; i++) {
				if(obj[i].activeSelf)
					obj[i].SetActive (false);

			}
		}
	}
}
