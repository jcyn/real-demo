﻿using UnityEngine;
using System.Collections;

public class MusicOn : MonoBehaviour {

	public GameObject Music;
	//public MovieTexture Movie;

	void OnClick(){
		if (Music.GetComponent<AudioSource> ().isPlaying) {
			Music.GetComponent<AudioSource> ().Pause ();
		} else {
			Music.GetComponent<AudioSource> ().Play();

		}
		//if(Movie.isPlaying)
			//Movie.Stop();
		
	}
}
