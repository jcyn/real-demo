﻿using UnityEngine;
using System.Collections;

public class Show : MonoBehaviour {

	public GameObject objShow;
	public GameObject objHide;
	/// <summary>
	/// 隐藏/展示对象
	/// </summary>
	void OnClick(){
		if (objHide.activeSelf) {
			objHide.SetActive (false);
		}
		if(!objShow.activeSelf)
			objShow.SetActive (true);
	}
	public void ShowIcon(){
		if(!objShow.activeSelf)
			objShow.SetActive (true);
		else
			objShow.SetActive (false);
	}
}
