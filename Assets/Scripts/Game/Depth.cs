﻿using UnityEngine;
using System.Collections;

public class Depth : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(this.GetComponent<UISprite>().depth < this.GetComponentInParent<UITexture>().depth)
		this.GetComponent<UISprite>().depth = this.GetComponentInParent<UITexture>().depth+1;
	}
}
