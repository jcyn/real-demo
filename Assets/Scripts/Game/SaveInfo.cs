﻿using UnityEngine;  
using System.Collections;  
using System.IO;  
using System.Collections.Generic;  
using System;  

public class SaveInfo : MonoBehaviour {  
	//文本中每行的内容  
	ArrayList infoall;  
	//皮肤资源，这里用于显示中文  
	private static SaveInfo instance;

	private SaveInfo()//私有化构造函数,
	{
		instance=this;
	}

	public static SaveInfo GetInstance()//凡是在外部调用这个类中的任何属性或方法都要通过该方法来调用,从而达到单例的效果
	{
		return instance;
	}

	/**
	 * * 写入信息到文本文件
   * path：文件创建目录
   * name：文件的名称
   *  info：写入的内容
   */  
	public void CreateFile(string path,string name,string info)  
	{  
		//文件流信息  
		StreamWriter sw;

		FileInfo t = new FileInfo(path+"//"+ name);  
		if(!t.Exists)  
		{  
			//如果此文件不存在则创建  
			sw = t.CreateText();  
		}  
		else  
		{  
			//如果此文件存在则打开  
			sw = t.AppendText();  
		}  
		//以行的形式写入信息  
		sw.WriteLine(info);  
		//关闭流  
		sw.Close();  
		//销毁流  
		sw.Dispose();  
	}  



	/**
   * 读取文本文件
   * path：读取文件的路径
   * name：读取文件的名称
   */  
	ArrayList LoadFile(string path,string name)  
	{  
		//使用流的形式读取  
		StreamReader sr =null;  
		try{  
			sr = File.OpenText(path+"//"+ name);  
		}catch(Exception e)  
		{  
			//路径与名称未找到文件则直接返回空  
			return null;  
		}  
		string line;  
		ArrayList arrlist = new ArrayList();  
		while ((line = sr.ReadLine()) != null)  
		{  
			//一行一行的读取  
			//将每一行的内容存入数组链表容器中  
			arrlist.Add(line);  
		}  
		//关闭流  
		sr.Close();  
		//销毁流  
		sr.Dispose();  
		//将数组链表容器返回  
		return arrlist;  
	}   


	/**
	 * 删除文件
   * path：删除文件的路径
   * name：删除文件的名称
   */  

	void DeleteFile(string path,string name)  
	{  
		File.Delete(path+"//"+ name);  
	}  



}  
