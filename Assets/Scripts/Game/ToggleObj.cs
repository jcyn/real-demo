﻿using UnityEngine;
using System.Collections;


public class ToggleObj : MonoBehaviour {
	public GameObject[] Eable;
	/// <summary>
	/// 对象开关
	/// </summary>
	void OnClick(){

		for (int i = 0; i < Eable.Length; i++) {
			if (i==0 && !Eable [i].activeSelf)
				Eable [i].SetActive (true);
			else if(i!=0 && Eable [i].activeSelf)
				Eable [i].SetActive (false);
			
		}
	}
}
