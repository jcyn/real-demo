﻿using UnityEngine;
using System.Collections;

public class SetDepth : MonoBehaviour {
	public GameObject PlaneObj;
	public int Dep;
	void OnClick(){
		PlaneObj.GetComponent<UITexture> ().depth = Dep;
	}
}
