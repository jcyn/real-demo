﻿using UnityEngine;
using System.Collections;

public class depthAdapt : MonoBehaviour {
	public GameObject obj;

	
	// Update is called once per frame
	void Update(){
		if(this.GetComponent<UITexture>().depth<=obj.GetComponent<UITexture>().depth){
			this.GetComponent<UITexture>().depth = obj.GetComponent<UITexture>().depth+1;
		}
	}
}
