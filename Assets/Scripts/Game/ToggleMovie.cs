﻿using UnityEngine;
using System.Collections;

public class ToggleMovie : MonoBehaviour {
	public MovieTexture movTexture;

	void OnClick(){
		
		if (!movTexture.isPlaying) {
			movTexture.Play ();
		} else {
			movTexture.Pause ();
		}
	}
}
