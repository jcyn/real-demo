﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System;
public class Keyboard : MonoBehaviour {
	[DllImport("user32.dll", CharSet = CharSet.Auto)]
	private static extern int SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int Width, int Height, int flags);
	[DllImport("user32.dll", CharSet = CharSet.Auto)]
	private static extern IntPtr GetForegroundWindow();
	[DllImport("user32.dll", EntryPoint = "FindWindow")]
	private extern static IntPtr FindWindow(string lpClassName, string lpWindowName);
	//public static bool isNull=true;
	//public GameObject[] obj;
	/// <summary>
	/// 置顶软键盘窗口
	/// </summary>
	public void keyboardUP() {
		IntPtr win = FindWindow("ThunderRT6FormDC", null);
		SetWindowPos(win, -1, 0, 0, 0, 0, 1|2);
		if (win==IntPtr.Zero) {
			//print("未找到置顶窗口");
		}
	}
	/// <summary>
	/// 输入框开始编辑
	/// </summary>
	public void StartEditor()
	{
		//Process.Start(Application.dataPath + "/jianpang.exe");
		//Process.Start(@"C:\Windows\winsxs\x86_microsoft-windows-osk_31bf3856ad364e35_6.1.7600.16385_none_aa93298fbb4246f2\osk.exe");
		//SetWindowPos(GetForegroundWindow(), -1, 0, 0, 0, 0, 1 | 2);
		//keyboardUP();
		Process.Start (Application.dataPath + "/Keyboard/Keyboard.exe");
	}
	/// <summary>
	/// 输入框编辑结束（结束软键盘进程）
	/// </summary>
	public void endEditor()
	{
		Process[] ps = Process.GetProcesses();//获取计算机上所有进程

		foreach (Process p in ps)
		{
			//print(p.ProcessName);
			if (p.ProcessName == "Keyboard")//判断进程名称"osk"
			{
				//print(p.ProcessName);
				p.Kill();//停止进程
				//isRunning =false;
				return;
			}
		}
		//}
	}
	/*public bool checkNull(){
		if (obj [0].GetComponent<UIInput> ().value != null && obj [1].GetComponent<UIInput> ().value != null)
			isNull = true;
		else
			obj [2].SetActive (true);
		
	}*/

}
