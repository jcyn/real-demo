﻿using UnityEngine;
using System.Collections;

public class PlayMovie : MonoBehaviour {

	//电影纹理
	public MovieTexture movTexture;
	public GameObject MusicPlane;
	void Start()
	{
		//设置当前对象的主纹理为电影纹理
	
		GetComponent<Renderer>().material.mainTexture = movTexture;
		movTexture.Play();
		//设置电影纹理播放模式为循环
		movTexture.loop = true;

		UIRoot root = GameObject.FindObjectOfType<UIRoot>();
		if (root != null) {
			float s = (float)root.activeHeight / Screen.height;
			int height =  Mathf.CeilToInt(Screen.height * s);
			int width = Mathf.CeilToInt(Screen.width * s);
			transform.localScale = new Vector3(width,height,1);
		}
	}
	void Update(){

			
		if (movTexture.isPlaying) {
			MusicPlane.GetComponent<AudioSource> ().Pause ();
		} else {
			MusicPlane.GetComponent<AudioSource> ().Play ();
		}
	
	}
	/// <summary>
	/// 停止播放
	/// </summary>
	public void StopMovie(){
		//if (movTexture.isPlaying)
			movTexture.Stop();
	}
	/// <summary>
	/// 播放
	/// </summary>
	public void PlayMov(){
		if (movTexture.isPlaying) {
			movTexture.Pause ();
		} else {
			movTexture.Play ();
		}
	}

}
