﻿using UnityEngine;
using System.Collections;

public class W8NGuiInputSetting : MonoBehaviour {
    public bool AutoCloseInput = true;
    public bool OSVersionCheck = true;
	void Start () {
        GetComponent<NguiInputForWin8>().AutoCloseInput = AutoCloseInput;
        GetComponent<NguiInputForWin8>().OSVersionCheck = OSVersionCheck;
	}

}
