﻿using UnityEngine;
using System.Collections;

public class W8UGuiInputSetting : MonoBehaviour {
    public bool AutoCloseInput = true;
    public bool OSVersionCheck = true;
    void Start()
    {
			GetComponent<UguiInputForWin8> ().AutoCloseInput = AutoCloseInput;
			GetComponent<UguiInputForWin8> ().OSVersionCheck = OSVersionCheck;
    }
}
