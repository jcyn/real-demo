﻿using System;
using UnityEngine;
using System.Collections;
using Win8InkForUnity;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System.Diagnostics;

[RequireComponent(typeof(W8NGuiInputSetting))]
[AddComponentMenu("NGUI/UI/NGui W8 Input Field")]
public class NguiInputForWin8 : UIInput
{
	[NonSerialized]
	public bool AutoCloseInput = true;
	[NonSerialized]
	public bool OSVersionCheck = true;
	[DllImport("user32.dll", CharSet = CharSet.Auto)]
	private static extern int SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int Width, int Height, int flags);
	[DllImport("user32.dll", CharSet = CharSet.Auto)]
	private static extern IntPtr GetForegroundWindow();
	[DllImport("user32.dll", EntryPoint = "FindWindow")]
	private extern static IntPtr FindWindow(string lpClassName, string lpWindowName);
	//private float duration = 3.0f;
	//private bool isRunning = false;
	protected override void OnSelect(bool isSelected)
	{
		base.OnSelect(isSelected);
		if (isSelected)
		{
			//float time = 0;

			//time += Time.deltaTime;
			//if (time > duration) {
				//time = 0;
				Process.Start (Application.dataPath + "/Keyboard/Keyboard.exe");
			//}
			//if (!isRunning) {

			//isRunning = true;
			//}
		}
		else
		{
			Process[] ps = Process.GetProcesses();//获取计算机上所有进程

			foreach (Process p in ps)
			{
				//print(p.ProcessName);
				if (p.ProcessName == "Keyboard" || p.ProcessName == "CKeyboardCm" )//判断进程名称"osk"
				{
					//print(p.ProcessName);
					p.Kill();//停止进程
					//isRunning =false;
					//return;
				}
			}
		}
	}
}