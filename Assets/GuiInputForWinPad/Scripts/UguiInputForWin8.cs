﻿using System;
using UnityEngine;
using System.Collections;
using Win8InkForUnity;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
[RequireComponent(typeof(W8UGuiInputSetting))]
[AddComponentMenu("UI/W8 Input Field")]
public class UguiInputForWin8 : InputField
{
    [NonSerialized]
    public bool AutoCloseInput = true;
    [NonSerialized]
    public bool OSVersionCheck = true;
    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        Ink.ShowW8InputPanel(OSVersionCheck);
    }
    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        if (AutoCloseInput) Ink.HideW8InputPanel();
    }
}
