﻿; Language File in UTF-8
; Comfort Keys 7.0
; Translated by Giorgio Tinelli
; 

;Language in format "italiano"
100000=Italiano (Italian)

;Language Identifier in Windows http://msdn2.microsoft.com/en-us/library/ms776294(VS.85).aspx
100001=$0410

;Common

000029=Disattiva
000030=Attiva
000034=%1 ti porge i suoi saluti!
000035=Puoi controllare le impostazioni tramite il menu contestuale.
000037=Programmi lanciati di recente
000038=Nessuna informazione sui programmi recenti
000067=Finestra "%1" in corso di apertura
000068=Attendi...
000080=Assegna  Comando-da-Tastiera...
000301=Errore imprevisto in %1:####%2
000305=Ti saremmo grati se volesti fornirci maggiori dettagli sull'errore.
000302=Riferisci l'errore
000395=Comando-da-Tastiera
000548=Comandi-da-Tastiera
000404=Ignoto
000396=Icona
000318=Vai al sito web
000319=Contatta l'Assistenza
000552=Richiedi nuove Funzionalita'?
000553=Forum
000321=Verif. esistenza nuova versione
000555=Nessuna nuova versione disponibile
000556=Nuova versione %1 disponibile. Vuoi scaricarla?
000862=Dai un voto a questo programma
000322=Info programma %1
000001=Impossibile impostare lo Hook!
000002=Non riesco a terminare lo Hook!
000026=Lo Handler è già in funzione!
000027=Procedura %1 non trovata!
000040=File %1 non trovato!
000028=Non riesco a caricare la DLL!
000561=%1 fa parte di "Comfort Keys" 

000303=Non mostrare piu' questo messaggio
000304=Errore nello spedire l'email
000324=Tutti (*.png;*.gif;*.pcx;*.ani;*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf)|*.png;*.gif;*.pcx;*.ani;*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf|Portable
 Network Graphics (*.png)|*.png|Immagine CompuServe GIF (*.gif)|*.gif|Immagine PCX
 (*.pcx)|*.pcx|Immagine ANI (*.ani)|*.ani|File Immagine JPG (*.jpg)|*.jpg|File Immagine
 JPEG (*.jpeg)|*.jpeg|Bitmaps (*.bmp)|*.bmp|Icone (*.ico)|*.ico|Metafiles avanzati
 (*.emf)|*.emf|Metafiles (*.wmf)|*.wmf

;Clipboard

000041=Appunti
000328=Gestore degli Appunti
000039=Non riesco a salvare gli Appunti
000042=Suono
000043=%1 Caratteri
000044=%1 Files
000045=%1 Kbytes
000082=Memoria Appunti vuota.
000466=Incolla come:
000637=Aggiungi ai Favoriti
000638=Sposta sopra
000639=Sposta sotto
000640=Storia
000641=Favoriti
000642=Filtro: %1
000643=Imposta Filtro
000811=Sessione attuale                                       
000644=Oggi
000645=Questa settimana
000646=Questo mese
000647=Testo
000749=-Tutti-
000756=Mostra anteprima
000776=Anteprima
000757=Mostra informazioni addizionali
000769=Cerca...
000779=Altezza dell'elemento:
000787=Sequenza di attivazione tasti
000788=Lista dei Files
000789=Testo HTML 
000790=Bitmap
000791=Metafile di Windows
000793=Incolla gli Appunti come testo semplice
000794=Incolla gli Appunti come sequenza attivazione tasti
000814=Sempre in primo piano
000815=Torna alla Gestione degli Appunti dopo l'incolla 
000829=Riunisci i frammenti di testo selezionati
000833=Cancella gli Appunti

;On-Screen Keyboard

000036=Tastiera-su-Schermo
000460=Scegli il tipo di tastiera
000018=Modifica il tipo di tastiera
000019=Salva il tipo di tastiera
000020=Scegli lo sfondo della tastiera
000022=Non reperito alcun tipo di tastiera!
000449=Modifica
000651=Clona
000468=Definizione della codifica di tasto
000017=Premi il tasto che vuoi aggiungere al layout
000450=Aggiungi lo sfondo...
000451=Annulla lo sfondo
000856=Imposta i bordi
000818=Mostra come sfondo
000821=Metti un bordo intorno al tasto
000822=Ridimensiona con un tasto
000452=Punti:
000875=Punti di divisione:
000453=Sinistra:
000454=In cima:
000455=Larghezza:
000456=Altezza:
000457=Codice Decimale:
000458=Codice Esadecimale:
000459=Descrizione:
000054=Da qui puoi creare la tua tastiera personalizzata. Guarda i tipi gia'? esistenti
 per capire i criteri di impostazione. Puoi cambiare grandezza e coordinate dei
 tasti evidenziati usando i tasti freccia e mantenendo nel contempo premuti i tasti
 Ctrl o Maiusc.


;Keys

000393=Tasti
000713=Backspace
000714=Tab
000715=Invio
000716=Maiusc
000741=Maiusc-destro
000742=Maiusc-sinistro
000717=Ctrl
000743=Ctrl-sinistro
000744=Ctrl-destro
000718=Alt
000719=AltGr
000720=Pausa
000721=BloccoMAIUSC
000722=Esc
000723=Spazio
000724=PgSu
000725=PgGiu'
000726=Fine
000727=Home
000728=FrecciaSN
000729=FrecciaSU
000730=FrecciaDS
000731=FrecciaGIU'
000732=StampaScr
000733=Ins
000734=Canc
000735=WinDS
000736=Win
000737=WinSN
000738=WinDS
000739=BloccoNum
000740=BloccoScorr


;Template Manager

000331=Modelli
000047=Modifica i Modelli
000048=Salva i Modelli
000307=Aggiungere Modelli dal file %1?
000308=Rimuovere i Modelli appena aggiunti al file %1?
000795=Aggiungi modello da Appunti
000478=Parola-chiave
000479=Testo semplice
000480=Testo in formato RTF
000481=Immagine
000487=%1 Modelli (*.ckt)|*.ckt|Tutti (*.*)|*.*
000488=Carattere
000489=Nome del Carattere
000490=Dimensione del Carattere
000491=Grassetto
000492=Corsivo
000493=Sottolineato
000494=Allinea a Sinistra
000495=Allinea a Destra
000496=Allinea al Centro
000497=Giustifica
000498=Colore del Carattere
000499=Colore di sfondo
000500=Elenchi puntati
000501=Entità della Sporgenza
000502=Entità del Rientro
000503=Apice
000504=Pedice
000505=Inserimento Immagine
000506=Inserimento Oggetto
000507=Ricerca Testo
000508=Parametri di Impostazione...
000532=Insermento Etichetta
000533=Inserisci Data Corrente
000534=Inserisci Ora Corrente
000535=Inserisci Data ed Ora Corrente
000536=Premi i Tasti
000551=Pausa (msec)
000537=Inserisci l'Etichetta <SomeOf> 
000538=Inserisci File di Testo
000539=Incolla Testo selezionato
000540=Incolla contenuto Appunti
000541=Formato Predefinito
000542=Formato Predefinito lungo
000543=Formato Predefinito corto
000055=Predisponi qui i frammenti di testo per la sostituzione. Specifica il nome
 del Modello ed immetti il frammento di testo interessato. Puoi anche definire la
 posizione del cursore a valle dell'inserimento del testo: basta inserire il simbolo
 '|' (slash verticale) nella posizione interessata.

;Macros

000061=Registrazione della Macro 
000062=Per terminare la registrazione, premi "Pausa" oppure clicca sulla icona Registrazione.
000063=Esecuzione della Macro 
000064=Per terminare l'esecuzione della Macro, premi "Pausa" o "Esc", o clicca sulla
 icona Esegui.

;Actions

000024=Impostazioni dei Comandi-da-Tastiera 
000522=Lista dei Comandi-da-Tastiera 
000023=Nessun Comando aggiuntivo
000025=Tipo di Azione
000414=Tipo di Azione:
000524=Attivo:
000525=In tutte le modalità di presentazione##Solo in modalità a-tutto-schermo##Esclusa
 modalità a-tutto-schermo
000031=L'azione è definita per un solo tasto
000032=La lista delle finestre nascoste è qui
000033=Il menu contestuale consente di sfogliare la lista delle finestre nascoste
 e rendere visibile quella di interesse.
000309=Aggiungere un insieme di Comandi-da-Tastiera prelevati dal file %1?
000310=Rimuovere un insieme di Comandi-da-Tastiera precedentemente prelevati dal
 file %1?

000201=Esegui il programma
000419=Nome/i del Programma, Cartella o Documento interessati (ciascuno in una linea
 separata):
000420=Seleziona il File...
000421=Seleziona la Cartella...
000422=Commuta al Programma già lanciato
000839=Inizia nella cartella:

000202=Apri URL
000423=Apri URL (per più URL, inseriscili in linee separate):
000424=Modalità di apertura dell'URL:

000203=Incolla il testo
000817=Mostra il testo nella Tastiera-su-Schermo

000205=Esegui la Macro-da-Tastiera
000632=Registra la Macro-da-Tastiera
000425=Registra
000427=Velocità di esecuzione:
000428=Numero delle ripetizioni:
000429=Attiva il processo (opzionale):

000605=Connettiti/Disconnettiti da una rete
000606=Connessione:

000213=%1 azioni
000210=Mostra la finestra delle impostazioni dei Comandi-da-Tastiera
000234=Attiva il file successivo dei Comandi-da-Tastiera
000206=Mostra il Gestore degli Appunti 
000204=Incolla testo da Modello
000209=Apri la finestra di commutazione dei tasks (mostra i documenti aperti e le
 applicazioni in esecuzione)
000233=Mostra/Nascondi la Tastiera-su-Schermo
000690=Attiva il tipo di Tastiera successivo
000207=Mostra la lista dei programmi lanciati di recente
000208=Mostra il desktop con i tasti "caldi" 
000315=Attiva il file dei Comandi-da-Tastiera
000827=Imposta la Lista delle Tastiere

000604=Controllo Audio 
000219=Aumenta il Volume
000220=Diminuisci il Volume 
000221=Volume on/off
000607=Espelli/Chiudi sportello CD 
000762=Drive:
000608=Entità del salto:
000837=Cambia l'applicazione predefinita per l'ascolto
000838=Cambia l'applicazione predefinita per la registrazione

000609=Controllo Monitor
000610=Spegnimento Monitor 
000611=Accensione Monitor
000612=Attiva Salvaschermo
000613=Impostazioni dello Schermo
000222=Aumenta Luminosità
000223=Diminuisci Luminosità

000614=Controllo della Finestra
000224=Minimizza la Finestra attiva
000225=Massimizza/Ripristina la Finestra attiva
000229=Chiudi la Finestra attiva
000230=Nascondi la Finestra attiva
000231=Mostra la Finestra nascosta per ultima
000763=Minimizza la finestra attiva nella Barra di Sistema
000764=Finestra attiva sempre in cima (on/off)
000765=Ripristina la finestra %1
000766=Chiudi la finestra %1

000226=Azioni di Sistema
000615=Apri la finestra "Rimozione sicura dell'hardware"
000616=Apri il Pannello di Controllo
000617=Apri le Proprietà "Data ed Ora"
000618=Apri le Proprietà di Internet
000619=Mappa il Disco di Rete
000620=Disconnetti il Disco di Rete
000621=Svuota il Cestino

000218=Cambia Lingua o MAIUSC/minusc
000214=Cambia la Lingua degli ultimi caratteri immmessi
000232=Cambia la Lingua di immissione del testo selezionato
000215=Commuta la Lingua di immissione
000633=Commuta la Lingua di immissione su quella specificata
000758=Utilizza la Lingua o il Layout specificato quando il tasto viene mantenuto
 abbassato
000759=Cambia la Lingua di immissione o il Layout dell'ultima parola scritta
000760=Cambia la Lingua di immissione o il Layout delle ultime due parole scritte
000761=Cambia la Lingua di immissione o il Layout delle ultime tre parole scritte
000216=Cambia il testo selezionato in "TUTTO MAIUSCOLO"
000217=Cambia il testo selezionato in "tutto minuscolo"
000631=Inverti MAIUSC/minusc nel testo selezionato 
000648=Cambia il testo selezionato in "Carattere di Sentence"
000649=Iniziale Maiuscola per ciascuna parola del testo selezionato
000634=Lingua o Formato Caratteri:

000622=Blocca/Riavvia/Spegni
000623=Blocca questo computer
000624=Cambia l'Utente
000625=Disconnetti
000626=Riavvia
000627=Sospendi
000628=Congela
000629=Spegni
000630=Mostra le alternative di arresto

000227=Blocca Tasto o Comando-da-Tastiera
000416=Abilita le combinazioni con altri Tasti 

000228=Sostituisci Tasto o Comando-da-Tastiera
000417=Sostituisci con il Tasto:

000412=Aggiungi l'insieme dal File...
000523=Combinazione di Tasti
000413=Combinazione di Tasti:
000876=Cambia la combinazione di chiavi
000415=Ultimo cambiamento effettuato:
000430=Azione:
000431=Predefinito##In Nuova Finestra
000484=%1 File (*.cka)|*.cka|Tutti (*.*)|*.*
000485=Tutti (*.*)|*.*|Applicazioni  (*.exe)|*.exe
000486=Favoriti  (*.url)|*.url|Tutti (*.*)|*.*
000746=Tutti (*.*)|*.*|Ini-files  (*.ini)|*.ini

000051=In questa finestra puoi definire i Comandi-da-Tastiera che saranno attivi
 in tutte le applicazioni. Premi la combinazione di tasti e specifica l'azione che
 ad essi vuoi associare. Puoi definire la combinazione anche utilizzando la Tastiera-su-Schermo.
 Puoi rivedere i Comandi-da-Tastiera già definiti richiamando il menu a tendina
 "Comandi-da-Tastiera".
000052=E' importante ricordare che quando si definiscono i Comandi-da-Tastiera i
 "tasti di controllo" destri e sinistri (cioè: Maiusc, Ctrl, Alt o Win) agiscono
 in maniera diversa. Se come Comandi-da-Tastiera vuoi usare solo quelli di destra
  premi quelli di destra. Se invece vuoi che entrambi abbiano la stessa funzione,
 allora premi quelli di sinistra.
000081=Puoi usare qualsiasi tasto, inclusi "Blocca tastiera numerica" e "Blocca
 Maiuscole". Ancora, puoi usare combinazioni particolari quali p.es. Alt+Tab.

;Options

000316=Opzioni

000325=Impostazioni Generali
000083=Queste Impostazioni definiscono le interazioni tra %1 e Windows. N.B.: il
 programma è più stabile se vi si associa un'alta priorità di esecuzione.
000335=Lingua:
000554=Trova altre traduzioni...
000336=Esegui %1 all'avvio di Windows
000337=Imposta alta priorità per %1
000636=Nascondi l'icona di %1 nella Barra di Sistema
000820=Mostra la %1 toolbar nella Barra Applicazioni
000338=Aggiungi al menu contestuale
000672=Mostra la tastiera alla schermata di logon
000873=Disabilita la tastiera "touch" di sistema ed il pannello di scrittura a mano
000874=Disabilita il servizio di sistema "%1"

000326=Tema
000085=Il Tema definisce l'aspetto delle finestre principali di %1.
000341=Stile:
000514=Standard
000342=Sistema
000509=Aqua
000510=Solare
000557=Trasparente
000511=Oscuro
000512=Nero
000513=Notte
000530=Oro
000751=Vista
000752=Office 2007 Blu
000753=Office 2007 Argento
000754=Office 2007 Nero
000755=MacOS
000674=Personalizzato
000652=Stile del disegno:
000653=Normale
000654=Sfumato
000655=Sfumato bombato
000343=Trasparenza dello sfondo:
000531=Trasparenza globale:
000344=Immagine di sfondo:
000345=Tipo di carattere:
000346=Carattere dei Tasti di Scrittura:
000673=Finestra in stile originario Windows
000864=Stile di Windows 8
000356=Mostra la didascalia
000786=Mostra i comandi della finestra
000863=Mostra lo sfondo

000705=Normale
000706="Caldo"
000707=Selezionato

000549=In questa finestra puoi impostare i Comandi-da-Tastiera che vuoi attivi per
 qualsiasi applicazione.
000086=Puoi aggiungere e rimuovere i Comandi-da-Tastiera standard: ciò non influenzerà
 gli altri, che verranno comunque preservati.
000327=Impostazione dei Comandi-da-Tastiera:
000347=1. Insieme Standard (tasto Win)
000518=2. Simboli Tipografici (Ctrl-destro e Ctrl-destro+Shift-destro)
000519=3. Tasti di Controllo per Windows Media Player (Home, Fine, PgSu, PgGiù)
000520=4. Disposizione tasti in Ordine alfabetico (ABC)

000087=Puoi riferirti alla Tastiera-su-Schermo per un utilizzo più intuitivo di
 tutti i Comandi-da-Tastiera. Definisci i rispettivi parametri di visualizzazione
 in modo che ti vengano mostrati quando ti interessa.
000350=Tipo di Tastiera:
000392=Descrizione del Tipo di Tastiera:
000351=Disposizione dei tasti:
000521=Nascondi automaticamente
000352=Mostra quando mantenuto premuto il tasto:
000770=Qualsiasi tasto
000353=Ritardo di comparsa (msec):
000354=Ritardo di scomparsa (msec):
000712=Mostra l'icona vicino all'area di immissione del testo (se possibile).
000767=Mostra la Tastiera-su-Schermo quando il cursore-testo è visibile.
000819=NON mostrare i Tasti premuti per la immissione di una password 
000772=Ancoraggio:
000773=Ancora al fondo del display
000774=Ancora alla sommità del display
000808=Puntatore del Mouse:
000809=Mano
000849=Sposta la tastiera-su-schermo su uno dei bordi dello schermo:
000850=Basso##Alto##Destra##Sinistra
000851=Rendi animato il lancio della tastiera-su-schermo, verso:
000852=il basso##l'alto##destra##sinistra
000853=Immetti i caratteri al rilascio dei tasti
000854=Mostra un'informazione sopra il tasto premuto
000872=Mostra caratteri/simboli alternativi
000855=Mostra i caratteri dei tasti in MAIUSCOLO
000859=Estensioni
000860=L'estensione del browser attiva la tastiera non appena si seleziona un campo
 di immissione di testo.
000861=Installa %1 l'estensione

000691=Mostra simboli ed icone al centro dei tasti 
000558=Mostra quale disposizione tasti predefinita:
000775=Mostra i simboli accessibili tramite Shift (con Shift non pressato e caratteri
 non grandi)
000563=Mostra i simboli accessibili via Ctrl e Ctrl+Maiusc
000355=Colora le aree di diteggiatura
000825=Le scritte sui Tasti di controllo sono nella lingua di immissione corrente
000826=Mostra lo stato dei tasti BloccoMaiusc, BloccoNum e BloccoScorr

000599=Gesti
000600=Usa i gesti
000601=Sensibilità ai gesti:
000602=Bassa
000603=Alta
000781=Imputazione veloce di maiuscole, spazi, etc.

000782=Zoom
000783=Adatta in larghezza
000784=Adatta in altezza
000810=Blocca le dimensioni della finestra   
000785=Usa lo zoom per ottenere una tastiera più grande o più piccola (migliorare
 la visibilità o vedere una porzione maggiore del desktop).

000798=Aiuto alla battitura
000799=Opzioni aggiuntive per facilitare la battitura.
000801=Abilita la ripetizione del tasto
000846=Ripeti il ritardo (msec):
000847=Ripeti la frequenza (caratteri al secondo):
000800=Tieni premuto il tasto sino al rilascio di:
000802=Abilita la funzione AutoClick
000803=Ritardo dell'AutoClick (msec):
000804=Imposta la maiuscola dopo:
000805=Elimina lo spazio prima:
000806=Aggiungi lo spazio dopo:
000807=Aggiungi lo spazio prima:
000848=Un doppio tocco della barra di spazio inserirà un punto seguito da uno spazio

000582=Icona Tastiera-su-Schermo
000710=Icona permanente, in primo piano
000583=Blocca la posizione
000584=Dimensione
000585=Trasparenza
000586=Opaco
000587=Puoi affinare i parametri di posizione attraverso il menu contestuale
000711=Associa l'icona al cursore del testo (se possibile).
000745=Mostra l'etichetta

000088=Puoi disabilitare l'aggiunta di frammenti identici a quelli già presenti
 nel Gestore degli Appunti. (Tieni presente che la presenza di grandi quantità
 di testo può rallentare l'esecuzione del programma).
000357=Mantieni traccia dei cambiamenti negli Appunti 
000792=Attiva/Disattiva la memoria dei cambiamenti negli Appunti
000358=Numero massimo dei frammenti salvati:
000359=Non aggiungere frammenti già presenti
000360=Anteprima del carattere di testo:
000679=Directory dei dati:
000780=Mostra (se possibile) il Gestore degli Appunti a fianco della successiva
 area di imputazione
000830=Disponi in cima i frammenti più vecchi (invece che quelli più nuovi)

000091=Puoi aggiungere e rimuovere i Modelli di immissione standard: ciò non influenzerà
 quelli aggiunti di recente, che verranno comunque preservati.
000365=Simboli nel nome del Modello:
000693=Sostituisci in automatico
000778=Mostra come griglia
000857=Quando trovi la chiave, mostra un suggerimento
000366=Insiemi standard:
000367=1. Espressioni di saluto e congedo
000515=2. Sigle ed Acronimi
000516=3. HTML
000517=4. Auto-Correzione (solo Inglese)
000529=5. Pascal
000834=File condiviso con i Modelli:

000680=Auto-completamento della parola
000796=Attiva l'auto-completamento della parola
000797=Attiva/Disattiva l'auto-completamento della parola
000681=Mentre scrivi una parola, il programma può fornire una lista di parole (a
 lui) note  che iniziano con i caratteri digitati.
000683=Soglia di attivazione (numero di caratteri):
000684=Numero delle parole mostrate:
000685=Aggiungi uno spazio dopo la parola completata
000686=Registra le parole di uso più frequente
000747=Conferma aggiunta termini al dizionario personale
000748=Clicca qui per aggiungere questo termine al dizionario personale
000692=Mostra i numeri
000687=Scarica
000688=Dizionari:
000689=Leggi/Ripristina la lista delle parole
000777=Conferma con il tasto:
000831=Mostra come bottoni a lato della Tastiers-su-Schermo (invece che come lista)
000832=Non mostrare parole ricondotte ad 1 solo carattere

000332=Commutatore di Lingua
000092=Le funzionalità di commutazione della Lingua restano attive anche dopo l'eventuale
 cambio del Tipo di Tastiera.
000368=Mostra bandierina invece che l'icona %1 nella Barra di Sistema
000369=Mostra bandierina accanto al cursore (ove possibile)
000370=Mostra bandierina solo dopo commutazione Lingua
000526=Inseguimento veloce di spostamenti complessi del cursore
000371=Livello trasparenza bandierina:
000598=Bandierina non presentabile in: Firefox, OpenOffice, CorelDRAW...
000694=Mostra questa icona quando il Blocco Maiuscole è attivato:
000650=Lingue e disposizioni-tasti abilitate alla commutazione:

000588=Barra della lingua
000589=Blocca la sua posizione
000590=Tipo della barra
000591=Solo Bandierina della Lingua
000592=Bandierina con Nome
000593=Lista verticale delle Bandierine
000594=Lista orizzontale delle Bandierine
000595=Lista delle Bandierine con Nome
000596=Livello di trasparenza della Barra-lingue
000597=Mostra, Nascondi e Sposta con la Tastiera-su-Schermo

000093=Le icone delle applicazioni associate ad un Comando-da-Tastiera sono mostrate
 sul tasto interessato (ciò ne facilita il reperimento).
000372=Mostra le icone dei Comandi-da-Tastiera Windows
000373=Mostra le icone dei Comandi-da-Tastiera delle applicazioni attive 
000374=Mostra le icone dei Comandi-da-Tastiera di %1 
000635=Mostra i Suggerimenti
000375=Mostra se i Comandi-da-Tastiera delle applicazioni sono disponibili

000329=Finestra di commutazione dei Task
000089=La finestra per la commutazione tra le applicazioni si attiva con il comando
 Alt+Tab (impostazione predefinita). Questa soluzione progettuale intende agevolare
 il lavoro di chi opera spesso con un alto numero di finestre aperte.
000361=Icone piccole
000362=Aggiungi le finestre nascoste in coda alla lista

000330=Registro storico dei Processi
000090=%1 registra lo storico di tutte le applicazioni portate in esecuzione. Questi
 dati potranno esserti utili per rendere più veloce il reperimento e l'attivazione
 di un programma già utilizzato.
000363=Numero max di programmi considerati:
000364=Non aggiungere programmi già presenti

000564=Suoni
000565=Per associare ad un evento un nuovo suono, clicca sull'evento interessato
 e poi seleziona il suono.
000566=Suono:
000567=Abilita i suoni
000576=Lista degli Eventi:
000577=Cerca su disco
000578=Ricerca su disco quale nuovo "%1" suono
000579=Files di tipo Wave (*.wav)
000568=Operazione di Tasti Alfanumerici da Tastiera-su-Schermo
000580=Operazione di Tasto "Indietro" o "Cancella" da Tastiera-su-Schermo
000569=Operazione di Tasti di Sistema da Tastiera-su-Schermo
000570=Operazione di Tasti Alfanumerici
000581=Operazione di Tasto "Indietro" o "Cancella"
000571=Operazione di Tasti di Sistema 
000572=Comando-da-Tastiera di %1 premuto
000574=Commutazione della Lingua di immissione o della Disposizione Tasti
000575=Sto Copiando negli Appunti

000708=Eccezioni
000709=Puoi specificare le applicazioni per le quali %1 deve essere disabilitata.

000878=Dipendenze
000879=%1 cambia le impostazioni in relazione all'applicazione correntemente attiva.

000840=Sicurezza
000841=Questi parametri forniscono funzionalità avanzate di sicurezza agli utilizzatori
 del programma.
000842=Nell'uscire, elimina dallo storico i frammenti:
000843=Tutti i frammenti
000844=Solo quelli che paiono delle password
000845=Disabilita l'autocompletamento quando si immettono delle passwords

000334=Avanzate
000094=In questa sezione sono prese in considerazione opzioni avanzate, per un affinamento
 ulteriore di %1.
000376=Stato del tasto Tastierina-Numerica:
000377=Non modificare##Attiva all'avvio##Disattiva all'avvio
000378=Tasti numerici attivi sempre
000379=Max numero di finestre nascoste:
000656=Azione abilitata cliccando sull'icona in Barra di Sistema:
000657=Mostra il menu contestuale con tutte le azioni disponibili
000658=Mostra le Impostazioni dei Comandi-da-Tastiera
000659=Mostra le Opzioni

000695=Imposta la password per le operazioni di cifratura...
000696=Password attuale:
000697=Nuova Password:
000698=Conferma Nuova Password:
000699=Nuova Password e la sua Conferma non coincidono!
000700=La password immessa non è stata accettata.
000701=La password deve avere min 3 e max 32 caratteri.
000702=Prego... riprova.
000703=Effettua la cifratura dei dati
000704=Decifra i dati
000816=Richiedi una password prima di iniziare

000472=Scegli un Comando-da-Tastiera
000473=Assegna il Comando-da-Tastiera corrente:
000474=Modifica il Comando-da-Tastiera corrente:

000065=Ripristinare le Impostazioni Predefinite?

;Forms

000006=(Nessuno)
000009=Errore
000010=ATTENZIONE!
000011=Info
000012=Conferma
000411=Crea Nuovo File
000407=Apri
000408=Files Recenti
000340=Salva
000409=Salva come...
000015=Salvare le modifiche?
000016=Salvare le modifiche prima di chiudere?
000105=Chiudi
000323=Esci
000014=Cancellare i dati?
000771=Cancellare il file %1?
000007=Formato del file non corretto: %1
000008=Impossibile creare la Cartella %1
000066=Il Testo '%1' non è stato trovato.
000418=Testo:
000101=OK
000670=Sì
000671=No
000102=Cancella
000103=Applica
000104=Aiuto
000306=Questo file esiste già!
000339=Ristabilisci i valori predefiniti
000348=Aggiungi...
000446=Aggiungi
000426=Modifica
000349=Rimuovi...
000447=Cancella
000559=Cancella tutti
000858=Taglia
000877=Copia
000560=Incolla
000397=Descrizione
000398=Verifica tutti
000828=Deseleziona tutti
000448=Seleziona tutti
000405=File
000406=Aiuto
000410=Invia per e-mail
000467=Non mostrare più questo messaggio
000482=File:
000483=Immagine:
000550=Valuta
000562=Accesso Negato
000675=Stabilisci\Modifica le dimensioni
000676=Scala
000677=Mantieni le proporzioni
000678=Dimensioni originarie
000682=Abilitato
000768=Disabilitato
000812=Gruppi                                                           
000813=Cancello il gruppo %1?
000823=Ripulisci
000824=Predefinito
000835=Annulla
000836=Ripristina
000865=Tutti i files
000866=Backups
000867=Backup Selezionato
000868=Ripristina
000869=Backup completato
000870=Ripristino effettuato
000871=%1 Elemento(i)

;Registration

000312=Acquista
000313=Registra
000475=Attivazione
000476=Nome:
000477=Chiave di Registrazione:
000750=Questa chiave di registrazione ? per la versione %1.##Vuoi scaricare adesso
 la versione %1 ?

000069=Immetti il Nominativo utilizzato per acquistare %1 e la Chiave di Registrazione.
   Per evitare errori, per quest'ultima ti consigliamo di utilizzare "copia" e "incolla".
000070=La Chiave di Registrazione non è corretta!
000071=Per completare l'attivazione, riavvia il programma.
000072=Il tempo di prova (di %1) è scaduto. Se vuoi continuare ad usare %1  devi
 perfezionarne l'acquisto.##  Il programma verrà chiuso tra %2 minuti!
000073=%1 è uno Shareware.
000074=L'utilizzo della versione non registrata avrà termine fra %1 giorni
000075=Il prodotto è fornito in licenza secondo i Termini e le Condizioni specificate
 nell'Accordo di Licenza. Intestatario della Licenza è:
000076=A valle della registrazione otterrai, a titolo gratuito:##+ Supporto tecnico
 per tutta la vita del prodotto.##+ Tutti gli Aggiornamenti standard del prodotto.##+
 Nessun limite temporale all'utilizzo e nè alcun noioso richiamo alla registrazione.
000544=+ Nuovi insiemi di Comandi-da-Tastiera.
000545=+ Nuovi insiemi di icone per i Richiami-automatizzati delle applicazioni.
000546=+ Nuovi insiemi di Modelli di immissione.
000547=+ Notifica di ogni Aggiornamento di ordine superiore, e la relativa Chiave
 di Registrazione.##+ Inoltre gli utenti privati potranno usare la propria licenza
 su tutti i propri computers.##Attraverso la registrazione ci permetterai infine
 di migliorare il nostro software e di continuare a sviluppare un prodotto ai massimi
 livelli di qualità!
000077=Ricordami tra:
000078=1 Giorno##3 Giorni##7 Giorni##15 Giorni
000079=Prova i nostri prodotti:

;Wizard

000380=Percorso guidato dell'installazione
000311=Hai finito con il Percorso guidato?
000381=< Indietro
000382=Avanti >
000383=Fine

000384=Saluti
000385=Selezione del tipo di Tastiera
000387=Demo del Commutatore di Lingua
000388=Demo del Gestore di Modelli
000389=Demo del Gestore di Appunti
000390=Fine

000391=%1 ti dà il Benvenuto!####Il percorso guidato all'installazione ti aiuterà
 ad impostare i parametri di base, in modo che ti sia possibile iniziare ad utilizzare
 da subito %1.####Scegli innanzitutto la Lingua di interfaccia.
000394=Utilizzato
000399=Stampa la lista

000527=Inibizione dei tasti in modalità a-tutto-schermo
000528=Blocca i tasti seguenti in modalità a-tutto-schermo.##(Può essere utile
 quando utilizzi giochi su computer).

000400=Scrivi una parola o frase qualsiasi e premi %1.##La Lingua di immissione
 o la disposizione dei caratteri verrà cambiata.####La disposizione cambia a monte
 dell'ultimo paragrafo immesso o iniziato.##Puoi premere anche più volte la combinazione
 di tasti specificata.

000401=1. Scrivi la parola "test" e premi %1. In questo modo la parola sarà sostituita
 da una frase presa dalla base dei Modelli senza ulteriori richieste.####2. Premi
 %1 e scrivi la parola "test" per scegliere il Modello di interesse. Premi il tasto
 Invio per inserire il Modello nel testo.

000402=Ora gli Appunti contengono l'informazione dimostrativa.##Premi %1 per vedere
 il contenuto degli Appunti.####Copia l'informazione di interesse negli Appunti:
 essa verrà così salvata per ogni necessità futura.

000403=Grazie. Hai terminato l'impostazione veloce.####Per affinare le impostazioni,
 clicca su "Opzioni".####Ti auguriamo un buon lavoro.##Comfort Software Group

;Edit Macros

000461=Modifica la Macro
000462=Ritardo
000463=Evento
000464=Tasto
000465=Esteso

;Icons

000333=Icone dei Comandi-da-Tastiera
000445=Insieme delle icone
000003=Modifica le icone dei Comandi-da-Tastiera
000004=Salva il file delle icone
000005=Nuovo Nome
000021=L'insieme delle icone non è stato trovato!

000432=Carica dal File...
000433=Istantanea dello Schermo...
000434=Incolla dagli Appunti
000435=Salve nel File...
000436=Copia negli Appunti
000437=Suggerimento:
000438=Cancella Icona e Suggerimento
000439=Colore Transparente:
000440=Applicazione
000441=Nome del File:
000442=Nome della Classe della Finestra principale:
000443=Nome dell'Applicazione:
000444=Scegli l'insieme delle icone

000469=Istantanea dello Schermo
000470=Effettua l'istantanea
000471=Dimensioni dell'istantanea

000053=In questa finestra puoi impostare le icone di richiamo diretto per qualsiasi
 applicazione. %1 riconoscerà automaticamente l'applicazione all'avvio utilizzando
 il nome del file .exe interessato (o quello della Classe della Finestra principale
 del programma). Puoi anche aggiungere una indicazione esplicita: essa verrà mostrata
 quando il puntatore del mouse passerà sul tasto interessato.
