﻿; Language File in UTF-8
; Comfort Keys 3.6
; Comfort Software Group
; 

;Language in format "Native (English)"
100000=Turkce (Turkish)

;Language Identifier in Windows http://msdn2.microsoft.com/en-us/library/ms776294(VS.85).aspx
100001=$041F

;Common

000029=Kapat
000030=Aç
000034=%1 den Merhaba!
000035=Ayarlar için sağ tık menüsünü kullanabilirsiniz.
000037=Son kullanılan programlar 
000038=Programlarla ilgili bilgi yok.
000067= "%1" Penceresi açılıyor
000068=Lütfen bekleyiniz...
000080=Klavye kısayolu ilişkilendir...
000301=Beklenmeyen bir hata oluştu  %1:####%2
000305=Hata raporu gönderirseniz memnun oluruz. 
000302=Hatayı rapor et
000395=Kısayol 
000548=Klavye kısayolları
000404=Bilinmiyor 
000396=Simge
000318=Web-site Aç
000319=Destek ekibi iletişim 
000552=İstekleriniz
000553=Forum
000321=Güncellemeleri denetle
000555=Yeni bir sürüm var
000556=%1 Yeni bir sürüm var. İndirmek ister misiniz?
000862=Rate this program
000322=%1 Hakkında 
000001=Unable to set Hook!
000002=Unable to stop Hook!
000026=Handler is already started!
000027=Procedure %1 is not found!
000040=Dosya %1 is not found!
000028=DLL Yüklenemiyor !
000561=%1, Comfort Keys'in parçasıdır 

000303=Bu hata mesajını gelecekte gösterme
000304=Email gönderirken hata! 
000324=Tümü (*.png;*.gif;*.pcx;*.ani;*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf)|*.png;*.gif;*.pcx;*.ani;*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf|Portable
 Network Graphics (*.png)|*.png|CompuServe GIF Image (*.gif)|*.gif|PCX Image (*.pcx)|*.pcx|ANI
 Image (*.ani)|*.ani|JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bitmaps
 (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf|Metafiles (*.wmf)|*.wmf

;Clipboard

000041=Pano
000328=Pano yöneticisi 
000039=Pano kaydedilemedi.
000042=Ses
000043=%1 Harfler 
000044=%1 Dosya
000045=%1 Kbytes
000082=Pano boş.
000466=Farklı yapıştır:
000637=Favorilere ekle
000638=Yukarı 
000639=Aşağı 
000640=Geçmiş
000641=Favoriler
000642=Filtre: %1
000643=Filtre oluştur
000811=This session
000644=Bugün 
000645=Bu hafta 
000646=Bu ay
000647=Metin 
000749=-Tümü-
000756=Ön izleme
000776=Preview
000757=Ek bilgi göster
000769=Araştır...
000779=Item height:
000787=Keystroke Sequence
000788=List of Files
000789=HTML Text
000790=Bitmap
000791=Windows Metafile
000793=Paste clipboard as plain text
000794=Paste clipboard as keystroke sequence
000814=Always On Top
000815=Focus returns to Clipboard Manager after pasting
000829=Merge Selected Text Fragments
000833=Clear Clipboard

;On-Screen Keyboard

000036=Ekran klavyesi
000460=Klavye tipi seç
000018=Klavye tipi düzenle
000019=Klavye tipi kaydet 
000020=Klavye arkaplan seç
000022=Hiçbir klavye tipi bulunamadı!
000449=Düzenle 
000651=Çoğalt
000468=Tuş kodu tanımla
000017=Eklemek istediğiniz tuşa basın
000450=Arkaplan ekle...
000451=Arkaplan sil
000856=Set borders
000818=Show as background
000821=Draw a border around a key
000822=Resize with a key
000452=Points:
000875=Dividing points:
000453=Sol:
000454=Üst:
000455=Genişlik:
000456=Yükseklik:
000457=Decimal Code:
000458=Hex Code:
000459=Açıklama:
000054=Bu pencerede her türlü klavye oluşturabilirsiniz. Daha iyi anlamak için
 mevcut klavyeleri inceleyin. Ctrl veya Shift tuşlarını kullanarak klavye ve
 tuş boyutlarını değiştirebilirsiniz. 


;Keys

000393=Ana denetim tuşu
000713=Backspace
000714=Tab
000715=Enter
000716=Shift
000741=LeftShift
000742=RightShift
000717=Ctrl
000743=LeftCtrl
000744=RightCtrl
000718=Alt
000719=AltGr
000720=Pause
000721=Caps
000722=Esc
000723=Space
000724=PgUp
000725=PgDn
000726=End
000727=Home
000728=Left
000729=Up
000730=Right
000731=Down
000732=PrtScr
000733=Ins
000734=Del
000735=Apps
000736=Win
000737=LeftWin
000738=RightWin
000739=NumLock
000740=ScrollLock


;Template Manager

000331=Şablon yöneticisi 
000047=Şablon düzenle
000048=Şablon kaydet 
000307=Dosyadan Şablon Ekle %1?
000308=Dosyadan Eklenen eski şablonları sil %1?
000795=Add template from clipboard
000478=Ad
000479=Düz Metin 
000480=Düzenlenmiş Metin 
000481=Resim 
000487=%1 Şablonlar  (*.ckt)|*.ckt|All (*.*)|*.*
000488=Yazıtipi 
000489=Yazıtipi adı
000490=Yazıtipi büyüklüğü 
000491=Kalın
000492=İtalik
000493=Alt çizgili 
000494=Sola yasla 
000495=Sağa yasla 
000496=Ortala
000497=İki yana yasla 
000498=Yazıtipi rengi 
000499=Arkaplan rengi 
000500=Bullets
000501=Outdents Block
000502=Indents Block
000503=Superscript
000504=Subscript
000505=Resim ekle
000506=Nesne ekle
000507=Metin bul
000508=Ayarlar...
000532=Insert Tag
000533=Tarih ekle
000534=Saat ekle
000535=Tarih ve saat ekle 
000536=Press Keys
000551=Pause (msec)
000537=Insert <SomeOf> Tag
000538=Metin dosyası ekle
000539=Seçilen metni yapıştır 
000540=Panodan yapıştır 
000541=Varsayılan biçim
000542=Uzun varsayılan biçim
000543=Kısa varsayılan biçim 
000055=Bu pencerede hazır metinleri şablonlara yerleştirebilirsiniz. Şablon
 adı belirleyin, metin girin. Ayrıca işlemi sonunda imleç pozisyonunu belirleyebilirsiniz.
 İmleçin olmasını istediğiniz yere  '|' sembolünü ekleyin.

;Macros

000061=Makro kayıt
000062=Kaydı durdurmak için "Pause" tuşuna basın veya Kayıt simgesine tıklayınız
 .
000063=Makro yürütme
000064=Makro yürütmeyi durdurmak için "Pause" tuşuna basın veya Yürüt simgesine
 tıklayınız ..

;Actions

000024=Klavye kısayolu ayarları
000522=Klavye kısayolu Liste
000023=İlave ayarlar yok
000025=Eylem tipi
000414=Eylem tipi:
000524=Etkin:
000525=Tüm ekran kipleri##Sadece tam ekran##Sadece tam ekran kipi dışında 
000031=Eylem sadece tek tuşa ayarlı
000032=Gizli pencereler
000033=Konteks menü, gizli pencereleri listeleyip onları görünür kılmanızı
 sağlar. 
000309=Klavye kısayolu seti yükle %1?
000310=%1 Dosyadan daha önce eklenen kısayollar kaldır? 

000201=Programı yürüt
000419=Program, dizin veya belge ismi (ayrı satırlara yaz):
000420=Dosya seç...
000421=Dizin seç...
000422=Çalışmakta olan programa geç
000839=Start in folder:

000202=URL Aç
000423=URL Aç (Birden fazla adres için ayrı satırlara yaz) :
000424=URL Tipi

000203=Metin yapıştır 
000817=Show text on the on-screen keyboard

000205=Tuşla, makro yürüt
000632=Tuşla makro kaydı
000425=Kayıt
000427=Yürütme hızı:
000428=Tekrar sayısı:
000429=İşlemi etkinleştir (optional):

000605=Ağ bağlantısı 
000606=Bağlantı:

000213=%1 eylemler
000210=Klavye kısayolları penceresi göster 
000234=Sonraki klavye kısayolu dosyasını etkinleştir 
000206=Pano yöneticisi göster
000204=Şablon yöneticisi göster
000209=Görev yöneticisi göster
000233=Sanal klavye göster/gizle 
000690=Sonraki klavye tipini etkinleştir 
000207=Son çalıştırılan uygulamaları listele 
000208=Masaüzerini Göster 
000315=Klavye kısayolu dosyası etkinleştir 
000827=Set the List of Keyboards

000604=Ses Denetimi 
000219=Sesi arttır 
000220=Sesi azalt
000221=Ses Aç/Kapat
000607=CD tepsisi Aç/Kapat
000762=Sürücü:
000608=Birim:
000837=Change the default playback device
000838=Change the default recording device

000609=Ekran Denetimi
000610=Ekranı kapat 
000611=Ekranı aç
000612=Ekran koruyucu çalıştır 
000613=Görüntü Ayarları 
000222=Parlaklık arttır 
000223=Parlaklık azalt

000614=Pencere Denetimi
000224=Etkin pencereyi simge durumuna küçült 
000225=Etkin pencereyi Maximize/Restore
000229=Etkin pencereyi kapat 
000230=Etkin pencereyi gizle 
000231=Show the last hidden window
000763=Etkin pencereyi sistem tepsisine gönder 
000764=Etkin pencere daima en üstte
000765=Önceki boyut %1
000766=Pencere kapat %1

000226=Sistem Eylemleri
000615=Pencere aç "Donanımı güvenle kaldır"
000616=Denetim masası aç
000617=Özellikler aç "Tarih ve saat"
000618=İnternet özellikleri 
000619=Ağ bağlantıları 
000620=Ağ sürücüsü bağlantı kes 
000621=Geridönüşüm kutusunu boşalt 

000218=Lisan veya Büyük/küçük harf değiştirir 
000214=Son yazdığınız harflerin giriş lisanını değiştir 
000232=Seçilen metin giriş lisanını değiştir 
000215=Giriş lisanını değiştir 
000633=Giriş lisanını belirlenen lisana değiştir 
000758=Tuşa basıldığında, belirtilen giriş dili veya kavye düzenini kullan
000759=Son yazılan kelimenin giriş dili veya kavye düzenini değiştir 
000760=Son yazılan iki kelimenin giriş dili veya kavye düzenini değiştir 
000761=Son yazılan üç kelimenin giriş dili veya kavye düzenini değiştir 
000216=Seçilen metni BÜYÜK HARF değiştir
000217=Seçilen metni küçük harf değiştir
000631=Seçilen metni bÜYÜK kÜÇÜK harf değiştir
000648=Seçilen metni Normal tümce düzeni değiştir.
000649=Seçilen metnin her kelimenin İlk Harfini Büyük Harf değiştir
000634=Lisan veya klavye düzeni:

000622=Kilitle/Yeniden başlat/Kapat
000623=Bilgisayarı kilitle 
000624=Kullanıcı değiştir 
000625=Oturumu kapat
000626=Yeniden başlat
000627=Uyku modu
000628=Hibernate
000629=Kapat
000630=Kapatma iletişim kutusu göster 

000227=Tuş veya kısayolu engelle 
000416=Diğer tuş kombinasyonuna izin ver

000228=Tuş veya kısayolu yer değiştir 
000417=Değiştirilecek tuş:

000412=Dosyadan set ekle...
000523=Tuş kombinasyonu 
000413=Tuş kombinasyonu:
000876=Change key combination
000415=Son değişim:
000430=Eylem:
000431=Varsayılan##Yeni pencerede 
000484=%1 Dosyaları (*.cka)|*.cka|All (*.*)|*.*
000485=All (*.*)|*.*|Uygulamalar  (*.exe)|*.exe
000486=Favorites  (*.url)|*.url|All (*.*)|*.*
000746=All (*.*)|*.*|Ini-files  (*.ini)|*.ini

000051=Bu pencerede herhangi bir uygulamaya kısayol tanımlayabilirsiniz. Tuş
 kombinasyonu basın, çalıştırılacak uygulamayı seçiniz. "Klavye Kısayolları"
 listesinde tüm kısayolları görebilirsiniz. 
000052=Dikkat! Sağ ve soldaki  (Shift, Ctrl, Alt veya Win) tuşları farklı olarak
 algılanır. 
000081=NumLock and CapsLock dahil her türlü tuşu kullanabilirsiniz. Ayrıca Alt+Tab
 gibi özel kombinasyon kullanabilirsiniz. 

;Options

000316=Seçenekler

000325=Genel özellikler
000083=Temel özellikleri ayarlamak suretiyle %1 ile Windows arasındaki uyumu sağlarsınız.
 Yüksek öncelik tanırsanız program performansı artacaktır.  
000335=Lisan:
000554=Başka dilde çeviri bul...
000336=%1 Windows başlangıçta çalıştır
000337=%1 için yüksek öncelik 
000636=%1 Sistem tepsisine gönder 
000820=Show the %1 toolbar in the taskbar
000338=Windows context menüye ekle
000672=Oturum Açma Ekranında Göster (oturum şifre) 
000873=Disable system touch keyboard and handwriting panel
000874=Disable system service "%1"

000326=Temalar
000085=Tema seçerek renk, stil, grafik vs gibi birçok görsel elementleri değiştirebilirsiniz.
000341=Stil:
000514=Standard
000342=System
000509=Aqua
000510=Summery
000557=Transparent
000511=Dark
000512=Black
000513=Night
000530=Gold
000751=Vista
000752=Office 2007 Blue
000753=Office 2007 Silver
000754=Office 2007 Black
000755=MacOS
000674=Kişisel
000652=Grafik stili:
000653=Normal
000654=Gradient
000655=Tube Gradient
000343=Arkaplan şeffaflık:
000531=Genel şeffaflık:
000344=Arkaplan resmi:
000345=Yazıtipi:
000346=Harf yazıtipi:
000673=Pencere özgün stili
000864=Windows 8 Style
000356=Başlık göster 
000786=Show the window buttons
000863=Show background

000705=Normal
000706=Hot
000707=Selected

000549=Herhangi bir uygulama için klavye kısayolu tanımlayabilirsiniz. 
000086=Standart klavye kısayolu ekleyip kaldırabilirsiniz.
000327=Klavye kısayolu seti:
000347=1. Standard set (on Win key)
000518=2. Typographical symbols (on RightCtrl and RightCtrl+RightShift)
000519=3. Control keys for Windows Media Player (on Home, End, PgUp, PgDn)
000520=4. English alphabetical order (ABC) of keyboard layout

000087=Sanal klavyeyi kısayol oluşturmak için kullanabilirsiniz.
000350=Klavye tipi:
000392=Klavye tipi açıklama:
000351=Keyboard layout:
000521=Otomatik gizle 
000352=Tuş basılıyken göster:
000770=Herhangi bir tuş 
000353=Görünüm gecikme (msec):
000354=Kaybolma gecikme (msec):
000712=Gezer pencere sanal klavyeyi metin giriş alanında göster
000767=Metin imleci varsa klavyeyi göster 
000819=When I enter a password don't show which keys are tapped
000772=Docking:
000773=Dock at Bottom of Screen
000774=Dock at Top of Screen
000808=Mouse pointer:
000809=Hand
000849=Move the on-screen keyboard to a screen border:
000850=Bottom##Top##Right##Left
000851=Animate the launch of the on-screen keyboard:
000852=Slide Down##Slide Up##Slide Right##Slide Left
000853=Type characters when keys are released
000854=Show a tip over a pressed character key
000872=Show alternate characters or symbols
000855=Show key characters in the UPPER CASE
000859=Extensions
000860=Browser extension brings up the on-screen keyboard when entering text input
 fields.
000861=Install %1 Extension

000691=Etiket ve simgeleri tuşlarda ortala
000558=Klavye yapısını varsayılan olarak göster:
000775=Show symbols accessible via Shift when Shift is not pressed and font is not
 large
000563=Show symbols accessible via Ctrl and Ctrl+Shift
000355=Klavyede parmak yerlerini renkli göster 
000825=Labels on control keys depend on the currently selected input language
000826=Show the state of Caps Lock, Num Lock and Scroll Lock keys

000599=Yüz ifadeleri
000600=Yüz ifadeleri kullan
000601=Yüz ifadeleri hassasiyet:
000602=Düşük 
000603=Yüksek 
000781=Fast entering of capital letters, spaces, etc.

000782=Zoom
000783=Fit width
000784=Fit height
000810=Lock window size
000785=You can zoom in to get a close-up view of your keyboard or zoom out to see
 more of the desktop.

000798=Typing aid
000799=Additional options for comfortable typing.
000801=Enable key repeat
000846=Repeat delay (msec):
000847=Repeat rate (characters per second):
000800=Hold down key until released:
000802=Enable AutoClick
000803=AutoClick delay (msec):
000804=Set capital after:
000805=Delete space before:
000806=Add space after:
000807=Add space before:
000848=Double taping the space bar will insert a period followed by a space

000582=Gezer pencere
000710=Görünür
000583=Kilitle
000584=Boyut
000585=Şeffaflık 
000586=Mat
000587=Gezer pencere ayarlarını konteks menü veya sağ tık menüsünden değiştirebilirsiniz.
000711=Gezer pencere sanal klavyeyi imleç pozisyonunda göster
000745=Başlık göster 

000088=Pano yöneticisi ile benzer metin parçalarını tekrar kopyalamak zorunda
 kalmazsınız. Performansı yavaşlatmaz. 
000357=Pano değişikliklerini izle 
000792=Track clipboard changes on/off
000358=Maximum number of saved fragments:
000359=Do not add identical fragments
000360=Preview text font:
000679=Data directory:
000780=Show Clipboard Manager next to the text entry area when possible
000830=Oldest fragments on top (otherwise newest on top)

000091=Standart şablonlar ekleyip kaldırabilirsiniz. önceki şablonlar saklanacaktır.
000365=Symbols interrupting template name:
000693=Auto replace
000778=Show as grid
000857=Show hint when keyword is found
000366=Standard template sets:
000367=1. Greetings and Farewells (English)
000515=2. Acronyms (English)
000516=3. HTML
000517=4. Auto-Correction (English)
000529=5. Pascal
000834=Shared file with templates:

000680=Otomatik kelime tamamlama 
000796=Enable Word Autocomplete
000797=Word autocomplete on/off
000681=Siz yazarken program listeden kelime tahmini yapabilir. 
000683=En az kaç harf:
000684=Görüntülenecek kelime sayısı:
000685=Kelime tamamlandığında boşluk ekle 
000686=Sık kullanılan kelimeleri öğren 
000747=Kişisel sözlüğünüze kelime eklenmesini onaylama 
000748=Bu kelimeyi kişisel sözlüğünüze eklemek için buraya tıklayınız.
000692=Rakamları göster 
000687=İndir 
000688=Sözlükler:
000689=Kelime listesini oku/yenile
000777=Confirm with key:
000831=Show as a set of buttons next to the on-screen keyboard (otherwise as a list)
000832=Do not show words with 1 character remaining

000332=Lisan değiştir
000092=Klavye planı değiştiğinde lisan özellikleri de değiştirilmiş olur.
000368= %1 yerine sistem tepsisinde bayrak göster
000369=Lisan bayrağını metin alanındaki işaretçi yanında göster *
000370=Lisan değişiminde sadece bayrağı göster 
000526=Accelerated tracking of the complex text cursor movements
000371=Bayrak şeffaflık:
000598=Bayrak, bazı uygulamalarda görünmeyebilir: Firefox, OpenOffice, CorelDRAW...
000694=Caps Lock simgesini göster:
000650=Değiştirilebilir Lisan ve yapılar :

000588=Lisan çubuğu 
000589=Lisan çubuğunu kilitle
000590=Lisan çubuğu türü
000591=Lisan bayrağı
000592=Lisan adı ve bayrağı
000593=Bayraklar altalta
000594=Bayraklar yanyana
000595=Bayraklar ve adlar
000596=Lisan çubuğu şeffaflık 
000597=Sanal klavye ile göste/gizle ve taşı 

000093=Uygulama kısayolu simgeleri kolaylık açısından burada görünür. 
000372=Windows kısayolu simgelerini göster 
000373=Etkin uygulama kısayolu simgelerini göster
000374=%1 kısayol simgelerini göster 
000635=Faydalı öneri göster
000375=Uygulama kısayolu tuşlarının kullanılabilirliğini göster

000329=Görev değiştirme penceresi 
000089=Uygulamalar arasında geçiş yapmak için varsayılan tuş kombinasyonu
 Alt+Tab.
000361=Küçük simgeler
000362=Gizli pencereleri liste sonuna ekle 

000330=Geçmiş penceresini işle
000090=%1 kullanılan uygulama geçmişini gösterebilir. Böylece sık kullanılan
 uygulamaları kolayca bulabilirsiniz. 
000363=İşlem geçmişi max uygulama sayısı:
000364=Benzer uygulamaları ekleme

000564=Sesler
000565=Uygulama için bir ses seçiniz 
000566=Ses:
000567=Sesler etkin
000576=Program eylemi:
000577=Araştır...
000578=Yeni "%1" ses için araştır
000579=Wave Files (*.wav)
000568=Alfanümerik tuş basıldı 
000580=BackSpace veya  Delete tuşu basıldı 
000569=Sistem tuşları kullanıldı 
000570=Alfanümerik tuş basıldı 
000581=BackSpace veya  Delete tuşu basıldı 
000571=Sistem tuşları kullanıldı 
000572=%1 Klavye kısayolu kullanıldı 
000574=Giriş lisanı veya lisan değiştirme 
000575=Panoya kopyalanıyor

000708=Özel durumlar
000709=%1 un etkin olmasını istemediğiniz uygulamaları belirtebilirsiniz. 

000878=Dependencies
000879=%1 changes the settings depending on the currently active application.

000840=Security
000841=These parameters provide advanced security functionality for program users.
000842=Delete fragments from history before shutdown:
000843=All fragments
000844=Fragments resembling passwords
000845=Disable the autocomplete function when entering passwords

000334=Gelişmiş
000094=Bu bölüm ileri seçenekleri içeriyor. 
000376=Num Lock durumu:
000377=Değişiklik yapma##Başlangıçta açık##Başlangıçta kapalı
000378=Numeric keys on the pad even if Num Lock is off
000379=Max gizli pencere sayısı :
000656=Sistem tepsisinde ikon tıklanınca yapılacak eylem:
000657=Tüm eylemlerde konteks menü göster 
000658=Klavye kısayolu ayarlarını göster
000659=Seçenekleri göster

000695=Şifre belirle...
000696=Şimdiki şifreniz:
000697=Şifre:
000698=Şifre doğrula:
000699=Eşleşmiyor.
000700=Şifre kabul edilmedi.
000701=Şifreniz 3-32 karakter olmalıdır.
000702=Tekrar deneyiniz.
000703=Veriyi şifrele
000704=Verinin şifresini çöz
000816=Request a password before starting

000472=Klavye kısayolu al
000473=Klavye kısayolu mevcut ilişki:
000474=Mevcut klavye kısayolu ilişki düzenle:

000065=Varsayılan ayarlar?

;Forms

000006=(Adsız)
000009=Hata
000010=Uyarı 
000011=Info
000012=Onayla
000411=Yeni dosya oluştur 
000407=Aç
000408=Son kullanılan 
000340=Kaydet 
000409=Farklı kaydet...
000015=Değişiklikleri kaydet?
000016=Kapatmadan önce değişiklikleri kaydet?
000105=Kapat
000323=Çıkış
000014=Kaydı sil?
000771=Delete file %1?
000007=Dosya türü hatalı: %1
000008=Dizin oluşturulamıyor  %1
000066=Metin '%1'bulunamadı.
000418=Metin:
000101=Tamam
000670=Evet
000671=Hayır
000102=İptal
000103=Uygula 
000104=Yardım 
000306=Dosya mevcut !
000339=Varsayılan ayarlar 
000348=Ekle...
000446=Ekle
000426=Düzenle
000349=Kaldır ...
000447=Sil
000559=Tümünü sil 
000858=Cut
000877=Copy
000560=Yapıştır 
000397=Açıklama 
000398=Tümünü işaretle
000828=Uncheck All
000448=Tümünü seç 
000405=Dosya
000406=Yardım 
000410=Mail ile gönder
000467=Bu mesajı gelecekte gösterme
000482=Dosya:
000483=Resim: 
000550=Deneme sürümü 
000562=Geçersiz 
000675=Ölçeklendir 
000676=Ölçeklendirme
000677=En/boy oranını koru 
000678=Original boyut
000682=Etkin 
000768=Etkin değil
000812=Groups
000813=Delete group %1?
000823=Clear
000824=Varsayılan
000835=Undo
000836=Redo
000865=All files
000866=Backups
000867=Backup Selected
000868=Restore
000869=Backup complete
000870=Restore complete
000871=%1 Item(s)

;Registration

000312=Online satınal...
000313=Lisans numarası kayıt
000475=Etkinleştirme 
000476=Ad:
000477=Lisans numarası:
000750=Bu Lisans numarası %1 sürüm için. ##Şimdi %1 sürümünü indirmek ister
 misiniz? 

000069=Lütfen adınızı giriniz (%1 Satın alırken bildirdiğiniz isim) ve lisans
 numarası giriniz. Kopyala/yapıştır daha uygun olur. 
000070=Lisans numarası geçersiz.
000071=Etkinleştirmek için yeniden başlat. 
000072=%1 Deneme süreniz doldu. %1 Programını kullanmaya devam etmek istiyorsanız
 satın almak zorundasınız. ##Program %2 dakika sonra kapanacak!
000073=%1 Programı Shareware.
000074=Lisans olmayan program %1 gün sonra kullanılamaz. 
000075=Ürün lisansınız için kullanıcı lisans sözleşmesindeki hükümler
 geçerlidir: 
000076=Lisanslı kullanıcıların avantajları:##+ Öncelikli teknik destek.##+
 Ücretsiz güncelleme .##+ Sınırsız kullanım, reklamsız menü.
000544=+ Free new standard sets of Keyboard Shortcuts.
000545=+ Ücretsiz uygulama kısayolları, simgeleri.
000546=+ Ücretsiz şablon setleri. 
000547=+ Önemli güncelleme ve sürüm yükseltme fırsatı, ücretsiz yeni Lisans
 numarası.##+ Evde kişisel kullanıcılar için tüm evdeki bilgisayarlar için
 lisans .##Ve tabii ki yazılımı geliştirmek ve mükemmel çözümler üretmek
 için bize destek ve motivasyon vermiş oluyorsunuz!
000077=Sonra hatırlat:
000078=1 Gün##3 Gün##7 Gün##15 Gün
000079=Ürünlerimizi deneyin:

;Wizard

000380=İlk kurulum sihirbazı 
000311=Kurulum sihirbazı ile bitir?
000381=< Geri
000382=İleri >
000383=Son

000384=Hoşgeldiniz  
000385=Klavye tipi seçimi
000387=Lisan değiştirme demo
000388=Şablonlar demo
000389=Pano yöneticisi demo
000390=Son

000391=%1 den selam!####İlk kurulum sihirbazına hoşgeldiniz. 
000394=Kullanılıyor
000399=Yazdır

000527=Tam ekranda tuş engelle 
000528=Tam ekranda şu tuşları engelle (Bilgisayar oyunlarında kullanılabilir)

000400=Herhangi bir kelime veya cümle yazın ve %1 basın.##Giriş lisanı veya
 yazılan harfler değişecektir. ####

000401=1. "Test" yazın ve %1 basın. Kelime, otomatik olarak uygun şablondan bir
 kelimeye dönüşür.####2. %1  basın ve "test"  yazın. Uygun şablonu seçin.
 Enter basın, metin alanına şablonu ekleyin.

000402=Panoda demo bilgileri bulunuyor.##%1 basın, pano içeriğini görüntüleyin.####

000403=Teşekkürler! İlk kurulumu tamamladınız.#### Diğer ayarlar için seçenekler
 tıklayınız.####Keyifli çalışmalar dileriz.##Comfort Software Group

;Edit Macros

000461=Makro düzenle 
000462=Gecikme 
000463=Eylem
000464=Key
000465=Extended

;Icons

000333=Kısayol simgeleri
000445=Simge seti
000003=Kısayol simgelerini düzenle
000004=Simge dosyasını kaydet 
000005=Yeni ad
000021=Simge seti bulunamadı!

000432=Dosyadan yükle...
000433=Ekran görüntüsü yakala...
000434=Panodan yapıştır 
000435=Dosyaya kaydet...
000436=Panodan yapıştır 
000437=Öneri:
000438=Simge ve önerileri sil
000439=Şeffaf renk:
000440=Uygulama 
000441=Dosya adı:
000442=Ana pencere Class Adı:
000443=Uygulama adı:
000444=Simge seti seç

000469=Ekran yakala
000470=Ekran yakala başlat
000471=Yakalama boyutu

000053=Bu pencerede herhangi bir uygulama için simge belirleyebilirsiniz. %1 otomatik
 olarak ilişkili uygulamayı çalıştıracaktır. Ayrıca fare pozisyonlardırma
 yapabilirsiniz. 
